# Maintainer: M0Rf30 <morf3089@gmail.com>
# Co-Maintainer: Ultra-azu <ultra.public@proton.me>
# Kernel config based on: defconfig, msm8953.config, and device configs

_flavor="postmarketos-qcom-msm8953"
pkgname=linux-$_flavor
pkgver=6.8.2
pkgrel=0
pkgdesc="Close to mainline linux kernel for Qualcomm Snapdragon MSM8953"
arch="aarch64"
_carch="arm64"
url="https://github.com/msm8953-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
"
makedepends="
	bash
	bison
	findutils
	flex
	openssl-dev
	perl
	postmarketos-installkernel
"

_tag="$pkgver-r0"

source="
	$pkgname-v$_tag.tar.gz::$url/archive/v$_tag.tar.gz
	config-$_flavor.aarch64
"
builddir="$srcdir/linux-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -Dm644 "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
8681cb70498797b4e53fa6a083355cf8a5e862733b5d8e89fcb0040691db15ec8af166c367f549e4191c7268336ccb18a0e24a07dfd899b764f2c93bfb8b70d8  linux-postmarketos-qcom-msm8953-v6.8.2-r0.tar.gz
11a1fee4eb92ae00b458b425bd9769ddeff5eec0309a652fd36bf784b2fabe33e4ab1793a0d6512c92164c9560fda3371333ed641f1b4ce025396a143de64a9e  config-postmarketos-qcom-msm8953.aarch64
"
